from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.forms import ExpenseCategoryForm, ReceiptForm
from receipts.models import Account, ExpenseCategory, Receipt

from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")


@login_required
def expense_category_create(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("home")
    else:
        form = ExpenseCategoryForm(request.POST)
    context = {"form": form}
    return render(request, "categories/create.html", context)


"""class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "categories/create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")"""


@login_required
def receipt_create(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.purchaser = request.user
            category.save()
            return redirect("home")
    else:
        form = ReceiptForm(request.POST)
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    context = {
        "categories": ExpenseCategory.objects.filter(owner=request.user)
    }
    return render(request, "categories/list.html", context)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
