from django.forms import ModelForm
from receipts.models import ExpenseCategory, Receipt


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "catgory", "account"]
