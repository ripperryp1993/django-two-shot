from django.contrib import admin

# Register your models here.
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptAdmin(admin.ModelAdmin):
    pass


class CategoryAdmin(admin.ModelAdmin):
    pass


class AccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)

admin.site.register(ExpenseCategory, CategoryAdmin)

admin.site.register(Account, AccountAdmin)
