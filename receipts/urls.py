from django.urls import path
from receipts.views import (
    AccountCreateView,
    AccountListView,
    # ExpenseCategoryCreateView,
    ReceiptListView,
    category_list,
    expense_category_create,
    receipt_create,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
    path("create/", receipt_create, name="receipt_create"),
    path(
        "categories/create/",
        expense_category_create,
        name="category_create",
    ),
    path("categories/", category_list, name="category_list"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
]
